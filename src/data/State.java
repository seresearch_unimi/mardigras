/**
 * This file is part of Mardigras.
 * 
 *  Mardigras is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Mardigras is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Mardigras. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package data;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;


import org.apache.hadoop.io.Writable;



public abstract class State implements java.io.Serializable, Writable, Cloneable {

	protected static final long serialVersionUID = -6906271731806002374L;
	private String name; // unique name of the state
	protected ArrayListWritable<Edge> incomingEdges;
	private boolean expanded=false;
	
	private static int counter = 0;
	
	public State(String unicID){
		name = unicID + counter++;
		incomingEdges = new ArrayListWritable<Edge>();
	}
	
	public State(){
		name= "";
		incomingEdges = new ArrayListWritable<Edge>();
	}
	
	@Override
	public abstract Object clone();
	
	/**
	 * create states directly reachable from the current state.
	 * The id must be forwarded to new states during their creation in order to assign them a unique name
	 * @return
	 */
	public abstract List<State> createSuccessors(String id, Model m);
	
	/**
	 * compute the actual relationship between this state and s.
	 * In case of concrete state spaces the possible relationships are just EQUALS or NONE,
	 * In case of abstract state spaces the relationship can be also INCLUDED or INCLUDES
	 * @param s
	 * @return
	 */
	public abstract Relationship identifyRelationship(State s);
	
	/**
	 * return f(S) computed on the current state as a String
	 * @return
	 */
	public abstract String getFeatures();

	@Override
	public void readFields(DataInput arg0) throws IOException {
		//name = WritableUtils.readCompressedString(arg0);
		name = arg0.readUTF();
		incomingEdges=new ArrayListWritable<Edge>();
		incomingEdges.readFields(arg0);
		expanded=arg0.readBoolean();
		
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		//WritableUtils.writeCompressedString(arg0, name);
		arg0.writeUTF(name);
		incomingEdges.write(arg0);
		arg0.writeBoolean(expanded);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Edge> getIncomingEdges(){
		return this.incomingEdges;
	}
	public void addIncomingEdge(Edge e){
		this.incomingEdges.add(e);
	}
	public void addIncomingEdges(List<Edge> edges){
		for(Edge e: edges)
			if(!this.incomingEdges.contains(e))
				this.incomingEdges.add(e);
	}
	
	public void setIncomingEdges(List<Edge> edges){
		this.incomingEdges.clear();
		this.incomingEdges.addAll(edges);
	}
	
	public void removeIncomingEdge(Edge e){
		this.incomingEdges.remove(e);
	}
	
	public boolean isExpanded(){
		return expanded;
	}
	public void setExpanded(){
		expanded=true;
	}
	
	public void setExpanded(boolean b){
		expanded=b;
	}
	
	@Override
	public boolean equals(Object s){
		return this.name.equals(((State)s).name);
	}
	
	// TEMP METHODS
	public abstract short[] get();

}
