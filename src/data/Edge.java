/**
 * This file is part of Mardigras.
 * 
 *  Mardigras is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Mardigras is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Mardigras. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package data;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;


public abstract class Edge implements java.io.Serializable, Writable {

	private static final long serialVersionUID = 3281656314556447147L;
	protected String source;
	protected String target;
	private EdgeType type;
	
	public Edge(){
		source=null;
		target=null;
		type = EdgeType.EE;
	}
	
	public abstract void addLabel(State src, State dst);
	
	public EdgeType getType() {
		return type;
	}
	public void setType(EdgeType ft){
		this.type=ft;
	}
	public void setType(char begin, char end){
		if(begin=='A' && end=='A')
			this.type=EdgeType.AA;
		else if(begin=='A' && end=='E')
			this.type=EdgeType.AE;
		else if(begin=='E' && end=='A')
			this.type=EdgeType.EA;
		else if(begin=='E' && end=='E')
			this.type=EdgeType.EE;
	}
	
	public char getBeginType(){
		if(this.type==EdgeType.AA || this.type==EdgeType.AE)
			return 'A';
		else // this.type==FLOWTYPE.EE || this.type==FLOWTYPE.EA
			return 'E';
	}
	
	public char getEndType(){
		if(this.type==EdgeType.AA || this.type==EdgeType.EA)
			return 'A';
		else // this.type==FLOWTYPE.EE || this.type==FLOWTYPE.AE
			return 'E';
	}

	@Override
	public void readFields(DataInput arg0) throws IOException {
		source=WritableUtils.readCompressedString(arg0);
		type=WritableUtils.readEnum(arg0, EdgeType.class);
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		WritableUtils.writeCompressedString(arg0, source);
		WritableUtils.writeEnum(arg0, type);	
	}

	public String getSource() {
		return source;
	}
	public String getTarget() {
		return target;
	}

	public void setTarget(State s) {
		target = s.getName();
	}
	public void setSource(State s) {
		source = s.getName();
	}
	
	public boolean equals(Object o){
		Edge e = (Edge)o;
		return this.source.equals(e.source);
	}

	abstract public Object clone();
}
