/**
 * This file is part of Mardigras.
 * 
 *  Mardigras is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Mardigras is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Mardigras. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package data;


import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;

import core.GenericGraphgen;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;



/**
 * State space (either concrete or abstract)
 * @author Matteo Camilli
 *
 */

public class Graph implements java.io.Serializable{
        
        private static final long serialVersionUID = 271534158749004517L;
        protected String name;
        protected ArrayList<State> nodes;
        protected ArrayList<Edge> edges;
        
        /**
         * build empty TRG
         */
        public Graph(){
                this.name="";
                this.nodes=new ArrayList<State>();
                this.edges=new ArrayList<Edge>();
        }
        /**
         * build a new graph with the root State only.
         * @param net
         */
        public Graph(Model model){
        	this();
    		nodes.add(model.buildRoot());
        }
        
        public String getName(){
                return this.name;
        }
        public void setName(String n){
                this.name=n;
        }
        
        public void addNode(State n){
                this.nodes.add(n);
        }
        
//      public synchronized void addNode(GraphNode node, GraphNode parent, String tr, FLOWTYPE type){
//              if(this.nodes.contains(parent)){
//                      this.nodes.add(node);
//                      Flow f=new Flow(parent, node, tr, type);
//                      this.flows.add(f);
//              }
//      }
        
       
        
        public void removeNode(State n){
                for(Edge e: this.getEdges(n))
                	this.edges.remove(e);
                this.nodes.remove(n);
        }
        
        public void addAll(ArrayList<State> nodes){
                this.nodes.addAll(nodes);
        }
        
        public void addEdge(Edge e){
                this.edges.add(e);
        }
        
//      public synchronized ArrayList<GraphNode> getPreset(GraphNode n){
//              ArrayList<GraphNode> list=new ArrayList<GraphNode>();
//              for(Flow f: this.flows){
//                      if(f.getTarget().equals(n))
//                              list.add(f.getSource());
//              }
//              return list;
//      }
        
//      public synchronized ArrayList<Flow> getFlows(GraphNode src, GraphNode dst){
//              ArrayList<Flow> list=new ArrayList<Flow>();
//              for(Flow f: this.flows){
//                      if(f.getSource().equals(src) && f.getTarget().equals(dst))
//                              list.add(f);
//              }
//              return list;
//      }
        
        public ArrayList<Edge> getEdges(State n){ //USATA SOLO IN UN PUNTO
                ArrayList<Edge> list=new ArrayList<Edge>();
                for(Edge e: this.edges){
                        if(e.getSource().equals(n) || e.getTarget().equals(n))
                                list.add(e);
                }
                return list;
        }
        
//      public synchronized ArrayList<Flow> getFlowsFrom(GraphNode src){
//              ArrayList<Flow> list=new ArrayList<Flow>();
//              for(Flow f: this.flows)
//                      if(f.getSource().equals(src))
//                              list.add(f);
//              return list;
//      }
        
        public ArrayList<State> getNodes(){
                return this.nodes;
        }
        
        public ArrayList<Edge> getIncomingEdges(State dst){
                ArrayList<Edge> list=new ArrayList<Edge>();
                for(Edge e: this.edges)
                        if(e.getTarget().equals(dst))
                                list.add(e);
                return list;
        }
        
        public State getRoot(){
                if(this.nodes.size()>0)
                        return this.nodes.get(0);
                else
                        return null;
        }
        
        public void createEdges(){
                for(State n: this.getNodes())
                        for(Edge e: n.getIncomingEdges()){
                                e.setTarget(n);
                                this.addEdge(e);
                        }
        }
        
        /**
         * Write output file in DFS.
         * @param path
         */
        public void writeOutputFile(String path, Configuration conf){
                System.out.println("\nWriting .graph file...");
                try {
                        FileSystem fs = FileSystem.get(new URI(GenericGraphgen.dfsUri), conf);
                        Path outFile = new Path(path);
                        OutputStream outStream = fs.create(outFile);
                        ObjectOutputStream objOutStream=new ObjectOutputStream(outStream);
                        objOutStream.writeObject(this);
                        objOutStream.close();
                } catch (Exception e) {
                        e.printStackTrace();
                }
                System.out.println("...DONE!");
        }
     
}
