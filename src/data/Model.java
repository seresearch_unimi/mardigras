/**
 * This file is part of Mardigras.
 * 
 *  Mardigras is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Mardigras is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Mardigras. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package data;

import java.io.InputStream;
import java.util.HashMap;



public interface Model {
	
	/**
	 * Build a Model object from a file input stream
	 * @param file
	 */
	public void buildFromFile(InputStream file);
	
	/**
	 * Build the root state from this model.
	 * Return null if the root state cannot be created.
	 * @return
	 */
	public State buildRoot();
	
	
	// TEMP METHODS
	public abstract HashMap<String, Integer> getPMap();
	public abstract HashMap<String, Integer> getTMap();
	
}
