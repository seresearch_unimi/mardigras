/**
 * This file is part of Mardigras.
 * 
 *  Mardigras is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Mardigras is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Mardigras. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package core.deadlock;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import data.Edge;
import data.State;

public class DeadlockMapper<E extends State> extends Mapper<Text, E, Text, E> {

	private E fake = null;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		//super.setup(context);
		String stateClassName = context.getConfiguration().get(DeadlockDiscovery.STATE_CLASS_NAME);
		try {
			Class<? extends State> stateClass = (Class<? extends State>) Class.forName(stateClassName);
			fake = (E)stateClass.newInstance();
			fake.setName("F");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void map(Text key, E value, Context context) throws IOException, InterruptedException {
		//super.map(key, value, context);
		for(Edge e: value.getIncomingEdges()){
			//System.out.println("ARC FROM: " + e.getSource());
			context.write(new Text(e.getSource()), fake);
		}
		//System.out.println("TO: " + value.getName());
		context.write(new Text(value.getName()), value);
	}

	

}
