/**
 * This file is part of Mardigras.
 * 
 *  Mardigras is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Mardigras is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Mardigras. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package core.deadlock;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;
import org.apache.hadoop.util.*;
import org.apache.hadoop.fs.*;

import core.GenericGraphgen;
import core.GenericMapper;
import core.KeepGoing;
import core.SimpleReducer;


import data.Edge;
import data.Graph;
import data.Model;
import data.State;

/**
 * Generic framework for large-scale state-space building implemented with an iterative mapreduce algorithm,
 * using hadoop-1.0.4
 * @author Matteo Camilli
 *
 */
public class DeadlockDiscovery extends Configured implements Tool {

	protected static final String REDUCED_DIR_NAME="reducedDir";
	protected static final String EDGE_CLASS_NAME="edgeClassName";
	protected static final String STATE_CLASS_NAME="stateClassName";
	protected static final String ITERATION_NUMBER="iterationNumber";
	protected static final String DFS_URI="dfsUri";

	public static String dfsUri = "";
	private String inputDir="";
	private String outputDir="";
	private String reducedDir="";
	private String bucket="";
	private boolean local = false;

	private String inputPath="";
	private int reducersNumber=1;

	private int iteration = 0;

	private data.Graph graph=null;
	private ArrayDeque<State> remainings;

	protected long inputSplitSize = 1048576*64; // 64MB


	public static enum MardigrasCounter {
		MAP_OUT_NEW_NODES,
		MAP_OUT_OLD_NODES,
		REDUCE_IN_NEW_NODES,
		REDUCE_IN_OLD_NODES,
		REDUCE_OUT_NEW_NODES,
		REDUCE_OUT_OLD_NODES,
		ARCS
	};

	protected static enum UpdateStatusCounter {
		UPDATE_STATUS
	};

	private Class<? extends State> stateClass;
	private Class<? extends Edge> edgeClass;
	private int mapSlots = 0;


	public DeadlockDiscovery(KeepGoing cond, Class<? extends State> cl, Class<? extends Edge> edge){
		stateClass = cl;
		edgeClass = edge;
		remainings = new ArrayDeque<State>();
		graph = new Graph();
	}


	public DeadlockDiscovery(Class<? extends State> cl, Class<? extends Edge> edge){
		this(new KeepGoing(), cl, edge);
	}

	public String getInputPath(){
		return inputPath;
	}

	public String getDfsUri(){
		return dfsUri;
	}

	public void setReducersNumber(int r){
		this.reducersNumber = r;
	}

	public void setInputSplitSize(int size){
		this.inputSplitSize = size;
	}

	public void setBucketName(String name){
		this.bucket = name;
	}

	public void setInputPath(String path){
		this.inputPath = path;
	}

	public void setGraph(data.Graph g){
		this.graph = g;
	}

	public boolean setRoot(State s){
		if(graph != null){
			graph.addNode(s);
			remainings.add(s);
			return true;
		}
		else
			return false;
	}

	public void setLocal(){
		this.local = true;
	}


	public void setMapSlots(int m){
		this.mapSlots  = m;
	}

	public void setFirstIteration(int i){
		this.iteration = i;
	}

	public void hdfsConfig(){

		if(bucket.isEmpty()){
			System.out.println("ERROR: Missing S3 Bucket name");
			System.exit(1);
		}

		if(local){
			dfsUri = "hdfs://localhost:9000/" + this.bucket;
			inputDir = dfsUri + "/input";
			outputDir = dfsUri + "/output";
			reducedDir = dfsUri + "/oldreduced";
		}
		else{
			dfsUri = "s3n://" + this.bucket;
			inputDir = dfsUri + "/input";
			outputDir = dfsUri + "/output";
			reducedDir = dfsUri + "/oldreduced";
		}
	}


	/**
	 * Effettua operazioni preliminari, prima di commissionare un nuovo job:
	 * elimina output dir, se esiste gia'
	 * 
	 * @param job
	 */
	public void cleanUp(Job job){
		try{
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// remove output dir.
			if(fs.exists(FileOutputFormat.getOutputPath(job)))
				fs.delete(FileOutputFormat.getOutputPath(job), true);
		}catch(IOException e){e.printStackTrace();}
	}

	public void cleanInput(Job job){
		try{
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// remove input dir.
			if(fs.exists(FileInputFormat.getInputPaths(job)[0]))
				fs.delete(FileInputFormat.getInputPaths(job)[0], true);
		}catch(IOException e){e.printStackTrace();}
	}

	public void cleanDir(Configuration conf, String dir){
		try{
			Path path=new Path(dir);
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), conf);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// remove dir.
			if(fs.exists(path))
				fs.delete(path, true);
		}catch(IOException e){e.printStackTrace();}
	}


	/**
	 * load TRG and remainings from output files.
	 * @param job
	 * @return
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public void loadOutput(Job job, Class<?> stateClass) throws IOException, InstantiationException, IllegalAccessException{
		graph=new Graph();
		graph.setName("out-graph");
		remainings=new ArrayDeque<State>();
		Path dir=new Path(reducedDir);
		ArrayList<String> oldReduced=new ArrayList<String>();

		FileSystem fs=null;
		try {
			fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SequenceFile.Reader reader=null;
		Text key=null;
		Text value=null;

		// get oldreduced nodes.
		if(fs.exists(dir))
			for(FileStatus fr: fs.listStatus(dir)){
				reader=new SequenceFile.Reader(fs, fr.getPath(), job.getConfiguration());

				key=new Text();
				value = new Text();

				while(reader.next(key, value)){
					oldReduced.add(value.toString());
					key=new Text();
					value = new Text();
				}
				reader.close();
			}

		// create trg.
		for(FileStatus f: fs.listStatus(FileOutputFormat.getOutputPath(job)))
			if(f.getPath().getName().startsWith("part-")){
				reader=new SequenceFile.Reader(fs, f.getPath(), job.getConfiguration());

				key=new Text();
				State val = (State)stateClass.newInstance();

				while(reader.next(key, val)){
					graph.addNode(val);

					if(!val.isExpanded())
						remainings.add(val);

					for(Edge e: new ArrayList<Edge>(val.getIncomingEdges()))
						if(oldReduced.contains(e.getSource()))
							val.removeIncomingEdge(e);

					key=new Text();
					val = (State)stateClass.newInstance();
				}
				reader.close();
			}
	}

	private long getInputSize(Job job){
		FileSystem fs=null;
		long size = 0;
		try {
			fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());

			for(FileStatus f: fs.listStatus(FileInputFormat.getInputPaths(job)))
				if(f.getPath().getName().startsWith("part-"))
					size += f.getLen();

		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch(IOException e){
			e.printStackTrace();
		}

		return size;
	}


	public int run() throws Exception{
		return ToolRunner.run(this, null);
	}
	
	private void sequentialDiscovery(){
		HashMap<State, List<Edge>> outgoingEdges = new HashMap<State, List<Edge>>();
		HashMap<String, State> states = new HashMap<String, State>();
		System.out.println("Reading input...");
		String inputDir = "/Users/matteo/Desktop/temp/";
		Configuration c = new Configuration();
		try {
			//FileSystem fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
			for(FileStatus f: FileSystem.getLocal(c).listStatus(new Path(inputDir))){
				if(f.getPath().getName().startsWith("part-")){
					SequenceFile.Reader reader = new SequenceFile.Reader(FileSystem.getLocal(c), f.getPath(), c);
					
					Text k = new Text();
					State v = stateClass.newInstance();
					while(reader.next(k, v)){
						System.out.println("Reading state: " + v.getName());
						outgoingEdges.put(v, new ArrayList<Edge>());
						states.put(v.getName(), v);
						v = stateClass.newInstance();
					}
					
					reader.close();
				}
			}
			System.out.println("DONE.");
			System.out.println("Running deadlock discovery...");
			int deadlocks = 0;
			for(State i: outgoingEdges.keySet()){
				for(Edge e: i.getIncomingEdges()){
					System.out.println("Edge from: " + e.getSource());
					State s = states.get(e.getSource());
					if(s==null)
						throw new IOException("Source not found in 'states': " + e.getSource());
					List<Edge> list = outgoingEdges.get(s);
					if(list==null)
						throw new IOException("State not found in 'outgoingEdges': " + s.getName());
					list.add(e);
				}
			}
			System.out.println("Outgoing edges...");
			for(State s: outgoingEdges.keySet()){
				List<Edge> list = outgoingEdges.get(s);
				System.out.println("From " + s.getName() + ": " + list.size());
				if(list.isEmpty())
					deadlocks++;
			}
			System.out.println("DONE!, Deadlocks: " + deadlocks);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} //catch (URISyntaxException e1) {
//			e1.printStackTrace();
//		}
	}
	
	
	private void createDeadlockFile(Job job){
		FileSystem fs=null;
		try {
			fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		SequenceFile.Reader reader=null;
		
		// Read deadlock states
		List<State> states = new ArrayList<State>();
		try {
			for(FileStatus f: fs.listStatus(FileOutputFormat.getOutputPath(job)))
				if(f.getPath().getName().startsWith("part-")){
					reader=new SequenceFile.Reader(fs, f.getPath(), job.getConfiguration());

					Text key=new Text();
					State val = (State)stateClass.newInstance();

					while(reader.next(key, val)){
						states.add(val);
						val = (State)stateClass.newInstance();
					}
				}
			
			if(!states.isEmpty()){
				// create deadlock state
				State deadlockState = (State)stateClass.newInstance();
				deadlockState.setName("D");
				Edge e = edgeClass.newInstance();
				e.setSource(deadlockState);
				deadlockState.addIncomingEdge(e);
				
				// create edges
				for(State s: states){
					e = edgeClass.newInstance();
					e.setSource(s);
					deadlockState.addIncomingEdge(e);
				}
				
				// write deadlock state
				String outDir = FileOutputFormat.getOutputPath(job).toString();
				SequenceFile.Writer writer = SequenceFile.createWriter(fs, job.getConfiguration(), 
						new Path( outDir + "/part-d"),
						Text.class, stateClass);
				writer.append(new Text("deadlock"), stateClass.cast(deadlockState));
				writer.close();
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}


	public int run(String [] args) throws Exception {
		
//		sequentialDiscovery(); // SEQ. ALGORITM TEST !!
//		System.exit(0);
		
		System.out.println("START: "+System.currentTimeMillis());

		this.hdfsConfig();
		int i= iteration;

		Job job = new Job(getConf());
		job.setJarByClass(DeadlockDiscovery.class);
		job.setJobName("deadlockdiscovery");

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(stateClass);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		job.setMapperClass(DeadlockMapper.class);
		job.setReducerClass(DeadlockReducer.class);

		job.setInputFormatClass(SequenceFileInputFormat.class);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);

		//SequenceFileInputFormat.setMinInputSplitSize(job, size);
		job.setNumReduceTasks(reducersNumber);
		FileInputFormat.setInputPaths(job, inputDir + i + "/*");
		FileOutputFormat.setOutputPath(job, new Path(inputDir + (i+1)));
		if(mapSlots != 0)
			inputSplitSize = this.getInputSize(job) / mapSlots;
		SequenceFileInputFormat.setMaxInputSplitSize(job, inputSplitSize);

		System.out.println("\n**************************");
		System.out.println(" DEADLOCK DISCOVERY ON INPUT: " + i);
		System.out.println("**************************");
		cleanDir(job.getConfiguration(), outputDir);
		job.getConfiguration().set(STATE_CLASS_NAME, stateClass.getName());

		boolean success = job.waitForCompletion(true);
		this.createDeadlockFile(job);

		System.out.println("STOP: "+System.currentTimeMillis());


		return success ? 0 : 1;
	}

} 
