/**
 * This file is part of Mardigras.
 * 
 *  Mardigras is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Mardigras is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Mardigras. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package core;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayDeque;
import java.util.ArrayList;

import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;
import org.apache.hadoop.util.*;
import org.apache.hadoop.fs.*;


import data.Edge;
import data.Graph;
import data.Model;
import data.State;

/**
 * Generic framework for large-scale state-space building implemented with an iterative mapreduce algorithm,
 * using hadoop-1.0.4
 * @author Matteo Camilli
 *
 */
public class GenericGraphgen extends Configured implements Tool {

	protected static final String INPUT_FILE_NAME="part-0";
	protected static final String REDUCED_DIR_NAME="reducedDir";
	protected static final String MODEL_CLASS_NAME="modelClassName";
	protected static final String EDGE_CLASS_NAME="edgeClassName";
	protected static final String STATE_CLASS_NAME="stateClassName";
	protected static final String ITERATION_NUMBER="iterationNumber";
	protected static final String DFS_URI="dfsUri";

	protected static int THRESHOLD = 200;
	protected static int HYSTERESIS = 50;

	public static String dfsUri = "";
	private String inputDir="";
	private String outputDir="";
	private String reducedDir="";
	private String outputFile="";
	private String bucket="";

	private String inputPath="";
	private int reducersNumber=1;
	private boolean local = false;
	private boolean fold = false;
	private boolean simple = false;
	private boolean combine = false;
	private boolean schimmy = false;
	private boolean imc = false; // in-map combining

	private int iteration = 0;

	private data.Graph graph=null;
	private ArrayDeque<State> remainings;

	protected long inputSplitSize = 1048576*64; // 64MB


	public static enum MardigrasCounter {
		MAP_OUT_NEW_NODES,
		MAP_OUT_OLD_NODES,
		REDUCE_IN_NEW_NODES,
		REDUCE_IN_OLD_NODES,
		REDUCE_OUT_NEW_NODES,
		REDUCE_OUT_OLD_NODES,
		ARCS
	};

	protected static enum UpdateStatusCounter {
		UPDATE_STATUS
	};

	private Class<? extends State> stateClass;
	private Class<? extends Model> modelClass;
	private Class<? extends Edge> edgeClass;
	private KeepGoing keepGoingCondition;
	private SequentialGraphBuilder localBuilder;
	private int mapSlots = 1;

	//optimizations
	public static final String SCHIMMY = "schimmy";
	public static final String IMC = "imc";


	public GenericGraphgen(KeepGoing cond, Class<? extends State> cl, Class<? extends Edge> edge, Class<? extends Model> model){
		keepGoingCondition = cond;
		stateClass = cl;
		edgeClass = edge;
		modelClass = model;
		localBuilder = null;
		remainings = new ArrayDeque<State>();
		graph = new Graph();
	}


	public GenericGraphgen(Class<? extends State> cl, Class<? extends Edge> edge, Class<? extends Model> model){
		this(new KeepGoing(), cl, edge, model);
	}


	public void setLocalBuilder(SequentialGraphBuilder b){
		localBuilder = b;
	}

	public String getInputPath(){
		return inputPath;
	}

	public String getDfsUri(){
		return dfsUri;
	}

	public void setTreshold(int l) {
		THRESHOLD = l;
	}

	public int getTreshold(){
		return THRESHOLD;
	}

	public void setHysteresis(int h){
		HYSTERESIS = h;
	}

	public int getHysteresis(){
		return HYSTERESIS;
	}

	public void setReducersNumber(int r){
		this.reducersNumber = r;
	}

	public void setLocal(){
		this.local = true;
	}

	public boolean isLocal(){
		return this.local;
	}

	public void setInputSplitSize(int size){
		this.inputSplitSize = size;
	}

	public void setBucketName(String name){
		this.bucket = name;
	}

	public void setInputPath(String path){
		this.inputPath = path;
	}

	public void setGraph(data.Graph g){
		this.graph = g;
	}

	public boolean setRoot(State s){
		if(graph != null){
			graph.addNode(s);
			remainings.add(s);
			return true;
		}
		else
			return false;
	}

	public void setFold(boolean fold){
		this.fold = fold;
	}

	public void setSimpleReducer(){
		this.simple = true;
	}

	public void useCombiner(){
		this.combine = true;
	}

	public void useSchimmy(){
		this.schimmy = true;
	}

	public void useInMapCombining(){
		this.imc = true;
	}

	public void setMapSlots(int m){
		this.mapSlots  = m;
	}

	public void setFirstIteration(int i){
		this.iteration = i;
	}

	public void hdfsConfig(){

		if(bucket.isEmpty()){
			System.out.println("ERROR: Missing S3 Bucket name");
			System.exit(1);
		}

		if(local){
			dfsUri = "hdfs://localhost:9000/" + this.bucket;
			inputDir = dfsUri + "/input";
			outputDir = dfsUri + "/output";
			reducedDir = dfsUri + "/oldreduced";
			outputFile = dfsUri + "/out-graph.graph";
		}
		else{
			dfsUri = "s3n://" + this.bucket;
			inputDir = dfsUri + "/input";
			outputDir = dfsUri + "/output";
			reducedDir = dfsUri + "/oldreduced";
			outputFile = dfsUri + "/out-graph.graph";
		}
	}


	/**
	 * Effettua operazioni preliminari, prima di commissionare un nuovo job:
	 * elimina output dir, se esiste gia'
	 * 
	 * @param job
	 */
	public void cleanUp(Job job){
		try{
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// remove output dir.
			if(fs.exists(FileOutputFormat.getOutputPath(job)))
				fs.delete(FileOutputFormat.getOutputPath(job), true);
		}catch(IOException e){e.printStackTrace();}
	}

	public void cleanInput(Job job){
		try{
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// remove input dir.
			if(fs.exists(FileInputFormat.getInputPaths(job)[0]))
				fs.delete(FileInputFormat.getInputPaths(job)[0], true);
		}catch(IOException e){e.printStackTrace();}
	}

	public void cleanDir(Configuration conf, String dir){
		try{
			Path path=new Path(dir);
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), conf);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// remove input dir.
			if(fs.exists(path))
				fs.delete(path, true);
		}catch(IOException e){e.printStackTrace();}
	}


	/**
	 * load TRG and remainings from output files.
	 * @param job
	 * @return
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public void loadOutput(Job job, Class<?> stateClass) throws IOException, InstantiationException, IllegalAccessException{
		graph=new Graph();
		graph.setName("out-graph");
		remainings=new ArrayDeque<State>();
		Path dir=new Path(reducedDir);
		ArrayList<String> oldReduced=new ArrayList<String>();

		FileSystem fs=null;
		try {
			fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SequenceFile.Reader reader=null;
		Text key=null;
		Text value=null;

		// get oldreduced nodes.
		if(fs.exists(dir))
			for(FileStatus fr: fs.listStatus(dir)){
				reader=new SequenceFile.Reader(fs, fr.getPath(), job.getConfiguration());

				key=new Text();
				value = new Text();

				while(reader.next(key, value)){
					oldReduced.add(value.toString());
					key=new Text();
					value = new Text();
				}
				reader.close();
			}

		// create trg.
		for(FileStatus f: fs.listStatus(FileOutputFormat.getOutputPath(job)))
			if(f.getPath().getName().startsWith("part-")){
				reader=new SequenceFile.Reader(fs, f.getPath(), job.getConfiguration());

				key=new Text();
				State val = (State)stateClass.newInstance();

				while(reader.next(key, val)){
					graph.addNode(val);

					if(!val.isExpanded())
						remainings.add(val);

					for(Edge e: new ArrayList<Edge>(val.getIncomingEdges()))
						if(oldReduced.contains(e.getSource()))
							val.removeIncomingEdge(e);

					key=new Text();
					val = (State)stateClass.newInstance();
				}
				reader.close();
			}
	}

	private long getOutputSize(Job job){
		FileSystem fs=null;
		long size = 0;
		try {
			fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());

			for(FileStatus f: fs.listStatus(FileOutputFormat.getOutputPath(job)))
				if(f.getPath().getName().startsWith("part-"))
					size += f.getLen();

		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch(IOException e){
			e.printStackTrace();
		}

		return size;
	}


	public void generateInputFile(Configuration conf, String fileName, String inputPath, data.Graph g, Class<?> stateClass) throws ClassNotFoundException{

		try {
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), conf);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			SequenceFile.Writer writer = SequenceFile.createWriter(fs, conf, new Path(inputPath+"/"+fileName),
					Text.class, stateClass);
			for(State n: g.getNodes())
				writer.append(new Text(n.getFeatures()), stateClass.cast(n));
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	/**
	 * load current model from the dfs.
	 * @param conf
	 * @param netPath
	 */
	public Model loadModelFromDfs(Configuration conf, String inputPath, String dfsUri){
		try{
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), conf);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			Path np=new Path(inputPath);
			if(!fs.exists(np)){
				System.out.println("ERROR: Invalid model path");
				System.exit(1);
			}
			else{
				Model model = null;
				try {
					model = modelClass.newInstance();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				InputStream is=fs.open(np);
				model.buildFromFile(is);
				is.close();
				return model;
			}

		}catch(IOException e){e.printStackTrace();}
		return null;
	}

	public int run() throws Exception{
		return ToolRunner.run(this, null);
	}


	public int run(String [] args) throws Exception {

		System.out.println("START: "+System.currentTimeMillis());


		//	   String inputFile = "/Users/matteo/Desktop/output/" + temporaryArg;
		//		Configuration c = new Configuration();
		//		int arcs = 0;
		//		try {
		//			SequenceFile.Reader reader = new SequenceFile.Reader(FileSystem.getLocal(c), new Path(inputFile), c);
		//			Text k = new Text();
		//			State v;
		//			v = stateClass.newInstance();
		//			while(reader.next(k, v))
		//				arcs += v.getIncomingEdges().size();
		//				
		//			reader.close();
		//		} catch (IOException e) {
		//			e.printStackTrace();
		//		}
		//		
		//		System.out.println("ARCS: " + arcs);



		this.hdfsConfig();

		DistributedCache.addCacheFile(new URI(getInputPath()), getConf());
		Model model = this.loadModelFromDfs(getConf(), getInputPath(), getDfsUri());
		this.setRoot(model.buildRoot());
		localBuilder = new SequentialGraphBuilder(model, THRESHOLD, edgeClass);

		boolean done=false;
		int i = iteration;

		while(!done){
			if(localBuilder != null){
				System.out.println("\n**************************");
				System.out.println(" START LOCAL BUILDER");
				System.out.println("**************************");

				done = localBuilder.build(graph, remainings);
			}
			if(!done){
				cleanDir(getConf(), inputDir + 0);
				cleanDir(getConf(), reducedDir);
				generateInputFile(getConf(), INPUT_FILE_NAME, inputDir + 0, graph, stateClass);

				boolean requireExpansion=true;
				while(requireExpansion){
					Job job = new Job(getConf());
					job.setJarByClass(GenericGraphgen.class);
					job.setJobName("distributedgraphgen");

					job.setOutputKeyClass(Text.class);
					job.setOutputValueClass(stateClass);

					job.setMapperClass(GenericMapper.class);
					if(simple){
						job.setReducerClass(SimpleReducer.class);
						if(combine)
							job.setCombinerClass(SimpleCombiner.class);
					}
					else{
						job.setReducerClass(GenericReducer.class);
						if(combine)
							job.setCombinerClass(GenericReducer.class);
					}


					job.setInputFormatClass(SequenceFileInputFormat.class);
					job.setOutputFormatClass(SequenceFileOutputFormat.class);

					//SequenceFileInputFormat.setMinInputSplitSize(job, size);  
					job.setNumReduceTasks(reducersNumber);
					FileInputFormat.setInputPaths(job, inputDir + i + "/part*");

					FileOutputFormat.setOutputPath(job, new Path(inputDir + (i+1)));

					SequenceFileInputFormat.setMaxInputSplitSize(job, inputSplitSize);

					if(schimmy)
						MultipleOutputs.addNamedOutput(job, "old", SequenceFileOutputFormat.class, Text.class, stateClass);

					System.out.println("\n**************************");
					System.out.println(" MAPRED ITERATION: " + i);
					System.out.println("**************************");
					System.out.println("INPUT PATH: " + inputDir + i + "/part*");
					
					cleanDir(job.getConfiguration(), outputDir);
					job.getConfiguration().set(REDUCED_DIR_NAME, reducedDir);
					job.getConfiguration().set(MODEL_CLASS_NAME, modelClass.getName());
					job.getConfiguration().set(EDGE_CLASS_NAME, edgeClass.getName());
					job.getConfiguration().set(STATE_CLASS_NAME, stateClass.getName());
					job.getConfiguration().setInt(ITERATION_NUMBER, i);
					job.getConfiguration().set(DFS_URI, dfsUri);
					job.getConfiguration().setBoolean(SCHIMMY, schimmy);
					job.getConfiguration().setBoolean(IMC, imc);
					i++;

					boolean success = job.waitForCompletion(true);
					
//					inputSplitSize = this.getOutputSize(job) / mapSlots;
//					System.out.println("SPLIT SIZE: " + inputSplitSize + " Bytes");
					// profiling
					long newNodes = job.getCounters().findCounter(MardigrasCounter.REDUCE_OUT_NEW_NODES).getValue();

					System.out.println("PROF: "+System.currentTimeMillis() + " " + newNodes);

					if(fold)
						requireExpansion = keepGoingCondition.evaluate(job, newNodes);
					else
						requireExpansion = newNodes > 0;

						if(!requireExpansion && fold && success)
							loadOutput(job, stateClass);
						else if(!success)
							return 1;
				}
				if(!fold)
					return 0;
			}
		}

		if(graph!=null){
			graph.createEdges();
			graph.writeOutputFile(outputFile, getConf());

			//  trg.writeDotFile(outputFile.replace(".graph", ".dot"), getConf());
		}

		return 0;
	}

} 
