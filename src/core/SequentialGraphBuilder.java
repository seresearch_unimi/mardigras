/**
 * This file is part of Mardigras.
 * 
 *  Mardigras is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Mardigras is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Mardigras. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package core;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;


import data.Edge;
import data.Graph;
import data.Model;
import data.Relationship;
import data.State;

/**
 * Sequential Builder.
 * @author Matteo Camilli
 *
 */
public class SequentialGraphBuilder {
	
//	private int stateNumber=1;
//	private int analysed=0;
//	private int included=0;
//	private int inclusing=0;
//	private int created=0;
//	private int equals=0;

	private Model model;
//	private String limit;
	private Graph graph;
	private ArrayDeque<State> remainingStates;
	private HashMap<Integer,ArrayList<State>> states;
	private Class<? extends Edge> edgeClass;
	
	private int threshold;
	
	//static private Logger log = Logger.getLogger("main.GraphBuilder");
	
	public SequentialGraphBuilder(Model model, int threshold, Class<? extends Edge> edgeClass){
		this.model=model;
//		this.limit=limit;
		this.threshold=threshold;
		this.edgeClass = edgeClass;
	}
	
	public void init(Graph graph, ArrayDeque<State> remainings){
		this.graph=graph;
		this.remainingStates=remainings;
		buildHashMap();
	}
	
	private void buildHashMap(){
		states=new HashMap<Integer,ArrayList<State>>();
		for(State n: graph.getNodes())
			insertInHashMap(n);
	}
	
	private void insertInHashMap(State n){
		//int idGroup=Utils.discriminantStrHash(n.getNormMark()).hashCode();
		int idGroup=n.getFeatures().hashCode();
		
		if(states.containsKey(idGroup))
			states.get(idGroup).add(n);
		else{
			ArrayList<State> l=new ArrayList<State>();
			l.add(n);
			states.put(idGroup, l);
		}
	}
	
	/**
	 * Builds the TRG starting from SymbolicState defined by tokens
	 * 'symbolicTime' fields and net's 'constraint' field in the net.
	 * This function does not check for Time Correctness of	the given
	 * net and can enter infinite loop.
	 * @throws Exception 
	 */
	public boolean build(Graph trg2, ArrayDeque<State> remainings) throws Exception{
		
		this.init(trg2, remainings);
		
		while(!remainingStates.isEmpty()){
			
			if(remainingStates.size()>threshold)
				return false;
			
			// profiling fronte espansione
			System.out.println("PROF: " + System.currentTimeMillis() + " " + remainingStates.size());
			
			State currentState = remainingStates.pollLast();
			currentState.setExpanded();
			
			System.out.println("Node still to expand: " + (remainingStates.size()+1));
			System.out.println("expand: " + currentState.getName());
			
			for(State newState: currentState.createSuccessors("", model)){
				int idGroup=newState.getFeatures().hashCode();
				
				Edge e = edgeClass.newInstance();
				e.setSource(currentState);
				e.addLabel(currentState, newState);
				newState.addIncomingEdge(e);
				
				int stateCase = 0;
				if(states.containsKey(idGroup)){
					for(State oldState: new ArrayList<State>(states.get(idGroup))){
						Relationship relationship = newState.identifyRelationship(oldState);
						if(relationship != Relationship.NONE){
							if(relationship == Relationship.INCLUDES){
								this.removeOldFlows(oldState);
								remainingStates.remove(oldState);
								states.get(idGroup).remove(oldState);
								graph.removeNode(oldState);
								stateCase = 1;
								break;
							}
							else{ // INCLUDED OR EQUALS
								stateCase = 2;
								if(relationship == Relationship.INCLUDED)
										e.setType(e.getBeginType(), 'E');
								currentState.addIncomingEdge(e);
								break;
						   }
						} 
					}	
				}
				if(stateCase < 2)
					insertNewNode(newState, stateCase == 1);
			}
			
		}
		
		System.out.println("...DONE.");
//		System.out.println("analysed: "+analysed);
//		System.out.println("created: "+created);
//		System.out.println("reduced: "+(included+inclusing+equals));
		return true;
	}
	
	
	private void removeOldFlows(State node){
		for(State n: graph.getNodes())
			for(Edge e: n.getIncomingEdges())
				if(e.getSource().equals(node)){
					n.removeIncomingEdge(e);
					break;
				}
	}

	
	/**
	 * Insert a node in the trg and in the queue of remaining states.
	 * @param newNode
	 * @param currentNode
	 * @param begin
	 * @param ec TODO
	 */
	private void insertNewNode(State state, boolean priority){
		//int idGroup=Utils.strHash(newNode.getNormMark()).hashCode();
		//newNode.identifyOverLimit(limit);
		graph.addNode(state);

		if(priority)
			remainingStates.addLast(state);
		else
			remainingStates.push(state);
		
		insertInHashMap(state);
	}
	
}
