/**
 * This file is part of Mardigras.
 * 
 *  Mardigras is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Mardigras is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Mardigras. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package core.ctl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import core.KeepGoing;
import data.Edge;
import data.Model;
import data.State;

public class EGLauncher extends Configured implements Tool {

	protected static final String REDUCED_DIR_NAME="reducedDir";
	protected static final String EDGE_CLASS_NAME="edgeClassName";
	protected static final String STATE_CLASS_NAME="stateClassName";
	protected static final String MODEL_CLASS_NAME="modelClassName";
	protected static final String ITERATION_NUMBER="iterationNumber";
	protected static final String DFS_URI="dfsUri";

	public static String dfsUri = "";
	private String inputDir="";
	private String outputDir="";
	private String reducedDir="";
	private String bucket="";
	private boolean local = false;

	private String inputPath="";
	private int reducersNumber=1;

	private int iteration = 0;

	protected long inputSplitSize = 1048576*64; // 64MB


	public static enum MardigrasCounter {
		MAP_OUT_NEW_NODES,
		MAP_OUT_OLD_NODES,
		REDUCE_IN_NEW_NODES,
		REDUCE_IN_OLD_NODES,
		REDUCE_OUT_NEW_NODES,
		REDUCE_OUT_OLD_NODES,
		ARCS
	};

	protected static enum UpdateStatusCounter {
		UPDATE_STATUS
	};

	private Class<? extends State> stateClass;
	private Class<? extends Edge> edgeClass;
	private Class<? extends Model> modelClass;
	private int mapSlots = 0;


	public EGLauncher(KeepGoing cond, Class<? extends State> cl, Class<? extends Edge> edge, Class<? extends Model> model){
		stateClass = cl;
		edgeClass = edge;
		modelClass = model;
	}


	public EGLauncher(Class<? extends State> cl, Class<? extends Edge> edge, Class<? extends Model> model){
		this(new KeepGoing(), cl, edge, model);
	}

	public String getInputPath(){
		return inputPath;
	}

	public String getDfsUri(){
		return dfsUri;
	}

	public void setReducersNumber(int r){
		this.reducersNumber = r;
	}

	public void setInputSplitSize(int size){
		this.inputSplitSize = size;
	}

	public void setBucketName(String name){
		this.bucket = name;
	}

	public void setInputPath(String path){
		this.inputPath = path;
	}

	public void setLocal(){
		this.local = true;
	}


	public void setMapSlots(int m){
		this.mapSlots  = m;
	}

	public void setFirstIteration(int i){
		this.iteration = i;
	}

	public void hdfsConfig(){

		if(bucket.isEmpty()){
			System.out.println("ERROR: Missing S3 Bucket name");
			System.exit(1);
		}

		if(local){
			dfsUri = "hdfs://localhost:9000/" + this.bucket;
			inputDir = dfsUri + "/input";
			outputDir = dfsUri + "/output";
			reducedDir = dfsUri + "/oldreduced";
		}
		else{
			dfsUri = "s3n://" + this.bucket;
			inputDir = dfsUri + "/input";
			outputDir = dfsUri + "/output";
			reducedDir = dfsUri + "/oldreduced";
		}
	}


	/**
	 * Effettua operazioni preliminari, prima di commissionare un nuovo job:
	 * elimina output dir, se esiste gia'
	 * 
	 * @param job
	 */
	public void cleanUp(Job job){
		try{
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// remove output dir.
			if(fs.exists(FileOutputFormat.getOutputPath(job)))
				fs.delete(FileOutputFormat.getOutputPath(job), true);
		}catch(IOException e){e.printStackTrace();}
	}

	public void cleanInput(Job job){
		try{
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// remove input dir.
			if(fs.exists(FileInputFormat.getInputPaths(job)[0]))
				fs.delete(FileInputFormat.getInputPaths(job)[0], true);
		}catch(IOException e){e.printStackTrace();}
	}

	public void cleanDir(Configuration conf, String dir){
		try{
			Path path=new Path(dir);
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), conf);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// remove dir.
			if(fs.exists(path))
				fs.delete(path, true);
		}catch(IOException e){e.printStackTrace();}
	}


	private long getInputSize(Job job){
		FileSystem fs=null;
		long size = 0;
		try {
			fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());

			for(FileStatus f: fs.listStatus(FileInputFormat.getInputPaths(job)))
				if(f.getPath().getName().startsWith("part-"))
					size += f.getLen();

		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch(IOException e){
			e.printStackTrace();
		}

		return size;
	}


	public int run() throws Exception{
		return ToolRunner.run(this, null);
	}
	
	
	/**
	 * load current model from the dfs.
	 * @param conf
	 * @param netPath
	 */
	public Model loadModelFromDfs(Configuration conf, String inputPath, String dfsUri){
		try{
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), conf);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			Path np=new Path(inputPath);
			if(!fs.exists(np)){
				System.out.println("ERROR: Invalid model path");
				System.exit(1);
			}
			else{
				Model model = null;
				try {
					model = modelClass.newInstance();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				InputStream is=fs.open(np);
				model.buildFromFile(is);
				is.close();
				return model;
			}

		}catch(IOException e){e.printStackTrace();}
		return null;
	}
	
	
	private void sequentialEG(Job job){
		HashMap<String, State> states = new HashMap<String, State>();
		ArrayList<State> prevOut = new ArrayList<State>();
		ArrayList<State> newOut = new ArrayList<State>();
		
		System.out.println("Reading input...");
		//String inputDir = "/Users/matteo/Desktop/input/";
		//Configuration c = new Configuration();
		try {
			FileSystem fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
			for(FileStatus f: fs.listStatus(new Path(inputDir + "12"))){
				if(f.getPath().getName().startsWith("part-")){
					SequenceFile.Reader reader = new SequenceFile.Reader(fs, f.getPath(), job.getConfiguration());
					
					Text k = new Text();
					State v = stateClass.newInstance();
					while(reader.next(k, v)){
						System.out.println("Reading state: " + v.getName());
						prevOut.add(v);
						v = stateClass.newInstance();
					}
					
					reader.close();
				}
				else{
					SequenceFile.Reader reader = new SequenceFile.Reader(fs, f.getPath(), job.getConfiguration());

					Text k = new Text();
					State v = stateClass.newInstance();
					while(reader.next(k, v)){
						System.out.println("Reading state: " + v.getName());
						states.put(v.getName(), v);
						v = stateClass.newInstance();
					}

					reader.close();
				}
			}
			System.out.println("DONE. TIME = " + System.currentTimeMillis());
			System.out.println("Running EG[_]...");
			int k = 0;
			do{
				System.out.println("****** ITERATION " + (k++));
				for(State i: prevOut){
					System.out.println("Cheking state " + i.getName() + "...");
					for(Edge e: i.getIncomingEdges()){
						System.out.print("Edge from: " + e.getSource() + "...");
						if(states.get(e.getSource()) != null){
							System.out.println("Found.");
							newOut.add(states.get(e.getSource()));
						}
						else
							System.out.println("Not found.");
					}
				}
				System.out.println("****** PREV-SIZE = " + prevOut.size() + ", NEW-SIZE = " + newOut.size());
				prevOut = newOut;
				newOut = new ArrayList<State>();
			}while(!prevOut.equals(newOut));
			
			System.out.println("Writing output... TIME = " + System.currentTimeMillis());
			SequenceFile.Writer writer = SequenceFile.createWriter(fs, job.getConfiguration(), 
					new Path(inputDir + "12/output"), Text.class, stateClass);
			for(State n: newOut){
				System.out.println("Writing " + n.getName());
				writer.append(new Text(n.getFeatures()), stateClass.cast(n));
			}
			writer.close();
			System.out.println("DONE!, " + newOut.size() + " states written. TIME = " + System.currentTimeMillis());

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} 
		
	}
	
	

	public int run(String [] args) throws Exception {
		
		System.out.println("START TIME = " + System.currentTimeMillis());

		this.hdfsConfig();
		
		
		int i= iteration;
		int k = 1;
		boolean keepGoing = true;
		long prevSize = 0;
		long newSize = 0;
		
		while(keepGoing){
			Job job = new Job(getConf());
			
//			sequentialEG(job); // SEQ. ALGORITM TEST !!
//			System.exit(0);
			
			job.setJarByClass(EGLauncher.class);
			job.setJobName("ctleg");

			job.setMapOutputKeyClass(Text.class);
			job.setMapOutputValueClass(stateClass);
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(stateClass);

			job.setMapperClass(EGMapper.class);
			job.setReducerClass(EGReducer.class);

			job.setInputFormatClass(SequenceFileInputFormat.class);
			job.setOutputFormatClass(SequenceFileOutputFormat.class);

			//SequenceFileInputFormat.setMinInputSplitSize(job, size);
			job.setNumReduceTasks(reducersNumber);
			FileInputFormat.setInputPaths(job, inputDir + i + "/*," + dfsUri + "/fixed/*");
			FileOutputFormat.setOutputPath(job, new Path(inputDir + (i+1)));
			if(mapSlots != 0)
				inputSplitSize = this.getInputSize(job) / mapSlots;
			SequenceFileInputFormat.setMaxInputSplitSize(job, inputSplitSize);
			
			//MultipleOutputs.addNamedOutput(job, "true", SequenceFileOutputFormat.class, Text.class, stateClass);

			System.out.println("\n**************************");
			System.out.println(" CTL FORMULA EG[_] ON INPUT: " + i + ", ITERATION: " + k);
			System.out.println("**************************");
			cleanDir(job.getConfiguration(), outputDir);
			job.getConfiguration().set(STATE_CLASS_NAME, stateClass.getName());

			boolean success = job.waitForCompletion(true);
			if(!success)
				return 1;
			
			newSize = job.getCounters()
					.findCounter("org.apache.hadoop.mapred.Task$Counter", "REDUCE_OUTPUT_RECORDS")
					.getValue();
			
			if(newSize == 0)
				keepGoing = false;
			else
				if(k >= 2){
					System.out.println("PREV-SIZE: " + prevSize + ", NEW-SIZE: " + newSize);
					if(prevSize == newSize)
						keepGoing = !this.testFixedPoint(i);
					System.out.println("FIXED-POINT: " + !keepGoing);
				}
			
			prevSize = newSize;
			i++;
			k++;
		}
		

		System.out.println("STOP: "+System.currentTimeMillis());


		return 0;
	}
	
	private boolean testFixedPoint(int i){
		return true;
	}

} 
