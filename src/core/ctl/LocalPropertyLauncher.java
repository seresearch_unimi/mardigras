/**
 * This file is part of Mardigras.
 * 
 *  Mardigras is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Mardigras is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Mardigras. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package core.ctl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayDeque;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import data.Model;
import data.State;

public class LocalPropertyLauncher extends Configured implements Tool {

	protected static final String REDUCED_DIR_NAME="reducedDir";
	protected static final String EDGE_CLASS_NAME="edgeClassName";
	protected static final String STATE_CLASS_NAME="stateClassName";
	protected static final String MODEL_CLASS_NAME="modelClassName";
	protected static final String ITERATION_NUMBER="iterationNumber";
	protected static final String DFS_URI="dfsUri";

	public static String dfsUri = "";
	private String inputDir="";
	private String outputDir="";
	private String reducedDir="";
	private String bucket="";
	private boolean local = false;

	private String inputPath="";
	private int reducersNumber=1;

	private int iteration = 0;

	private data.Graph graph=null;
	private ArrayDeque<State> remainings;

	protected long inputSplitSize = 1048576*64; // 64MB


	public static enum MardigrasCounter {
		MAP_OUT_NEW_NODES,
		MAP_OUT_OLD_NODES,
		REDUCE_IN_NEW_NODES,
		REDUCE_IN_OLD_NODES,
		REDUCE_OUT_NEW_NODES,
		REDUCE_OUT_OLD_NODES,
		ARCS
	};

	protected static enum UpdateStatusCounter {
		UPDATE_STATUS
	};

	private Class<? extends State> stateClass;
	private Class<? extends Model> modelClass;
	private int mapSlots = 0;


	public LocalPropertyLauncher(Class<? extends State> cl, Class<? extends Model> model){
		stateClass = cl;
		modelClass = model;
	}

	public String getInputPath(){
		return inputPath;
	}

	public String getDfsUri(){
		return dfsUri;
	}

	public void setReducersNumber(int r){
		this.reducersNumber = r;
	}

	public void setInputSplitSize(int size){
		this.inputSplitSize = size;
	}

	public void setBucketName(String name){
		this.bucket = name;
	}

	public void setInputPath(String path){
		this.inputPath = path;
	}

	public void setGraph(data.Graph g){
		this.graph = g;
	}

	public boolean setRoot(State s){
		if(graph != null){
			graph.addNode(s);
			remainings.add(s);
			return true;
		}
		else
			return false;
	}

	public void setLocal(){
		this.local = true;
	}


	public void setMapSlots(int m){
		this.mapSlots  = m;
	}

	public void setFirstIteration(int i){
		this.iteration = i;
	}

	public void hdfsConfig(){

		if(bucket.isEmpty()){
			System.out.println("ERROR: Missing S3 Bucket name");
			System.exit(1);
		}

		if(local){
			dfsUri = "hdfs://localhost:9000/" + this.bucket;
			inputDir = dfsUri + "/input";
			outputDir = dfsUri + "/output";
			reducedDir = dfsUri + "/oldreduced";
		}
		else{
			dfsUri = "s3n://" + this.bucket;
			inputDir = dfsUri + "/input";
			outputDir = dfsUri + "/output";
			reducedDir = dfsUri + "/oldreduced";
		}
	}


	public void cleanDir(Configuration conf, String dir){
		try{
			Path path=new Path(dir);
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), conf);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// remove dir.
			if(fs.exists(path))
				fs.delete(path, true);
		}catch(IOException e){e.printStackTrace();}
	}


	private long getInputSize(Job job){
		FileSystem fs=null;
		long size = 0;
		try {
			fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());

			for(FileStatus f: fs.listStatus(FileInputFormat.getInputPaths(job)))
				if(f.getPath().getName().startsWith("part-"))
					size += f.getLen();

		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch(IOException e){
			e.printStackTrace();
		}

		return size;
	}


	public int run() throws Exception{
		return ToolRunner.run(this, null);
	}
	
	
	/**
	 * load current model from the dfs.
	 * @param conf
	 * @param netPath
	 */
	public Model loadModelFromDfs(Configuration conf, String inputPath, String dfsUri){
		try{
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), conf);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			Path np=new Path(inputPath);
			if(!fs.exists(np)){
				System.out.println("ERROR: Invalid model path");
				System.exit(1);
			}
			else{
				Model model = null;
				try {
					model = modelClass.newInstance();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				InputStream is=fs.open(np);
				model.buildFromFile(is);
				is.close();
				return model;
			}

		}catch(IOException e){e.printStackTrace();}
		return null;
	}


	public int run(String [] args) throws Exception {
		
		System.out.println("START TIME = " + System.currentTimeMillis());

		this.hdfsConfig();
		
		DistributedCache.addCacheFile(new URI(getInputPath()), getConf());
		int i= iteration;

		Job job = new Job(getConf());
		job.setJarByClass(LocalPropertyLauncher.class);
		job.setJobName("localproperty");

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(stateClass);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(stateClass);

		job.setMapperClass(LocalPropertyMapper.class);
		job.setReducerClass(LocalPropertyReducer.class);

		job.setInputFormatClass(SequenceFileInputFormat.class);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);

		//SequenceFileInputFormat.setMinInputSplitSize(job, size);
		job.setNumReduceTasks(reducersNumber);
		FileInputFormat.setInputPaths(job, inputDir + i + "/*");
		FileOutputFormat.setOutputPath(job, new Path(inputDir + (i+1)));
		if(mapSlots != 0)
			inputSplitSize = this.getInputSize(job) / mapSlots;
		SequenceFileInputFormat.setMaxInputSplitSize(job, inputSplitSize);
		
		MultipleOutputs.addNamedOutput(job, "true", SequenceFileOutputFormat.class, Text.class, stateClass);

		System.out.println("\n**************************");
		System.out.println(" Local Property evaluation on input: " + i);
		System.out.println("**************************");
		cleanDir(job.getConfiguration(), outputDir);
		job.getConfiguration().set(MODEL_CLASS_NAME, modelClass.getName());

		boolean success = job.waitForCompletion(true);
		//this.createDeadlockFile(job);

		System.out.println("STOP: "+System.currentTimeMillis());


		return success ? 0 : 1;
	}

} 
