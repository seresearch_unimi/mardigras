/**
 * This file is part of Mardigras.
 * 
 *  Mardigras is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Mardigras is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Mardigras. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package core.ctl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import core.KeepGoing;
import data.Edge;
import data.Graph;
import data.Model;
import data.State;

public class EXLauncher extends Configured implements Tool {

	protected static final String REDUCED_DIR_NAME="reducedDir";
	protected static final String EDGE_CLASS_NAME="edgeClassName";
	protected static final String STATE_CLASS_NAME="stateClassName";
	protected static final String MODEL_CLASS_NAME="modelClassName";
	protected static final String ITERATION_NUMBER="iterationNumber";
	protected static final String DFS_URI="dfsUri";

	public static String dfsUri = "";
	private String inputDir="";
	private String outputDir="";
	private String reducedDir="";
	private String bucket="";
	private boolean local = false;

	private String inputPath="";
	private int reducersNumber=1;

	private int iteration = 0;

	private data.Graph graph=null;
	private ArrayDeque<State> remainings;

	protected long inputSplitSize = 1048576*64; // 64MB


	public static enum MardigrasCounter {
		MAP_OUT_NEW_NODES,
		MAP_OUT_OLD_NODES,
		REDUCE_IN_NEW_NODES,
		REDUCE_IN_OLD_NODES,
		REDUCE_OUT_NEW_NODES,
		REDUCE_OUT_OLD_NODES,
		ARCS
	};

	protected static enum UpdateStatusCounter {
		UPDATE_STATUS
	};

	private Class<? extends State> stateClass;
	private Class<? extends Edge> edgeClass;
	private Class<? extends Model> modelClass;
	private int mapSlots = 0;


	public EXLauncher(KeepGoing cond, Class<? extends State> cl, Class<? extends Edge> edge, Class<? extends Model> model){
		stateClass = cl;
		edgeClass = edge;
		modelClass = model;
		remainings = new ArrayDeque<State>();
		graph = new Graph();
	}


	public EXLauncher(Class<? extends State> cl, Class<? extends Edge> edge, Class<? extends Model> model){
		this(new KeepGoing(), cl, edge, model);
	}

	public String getInputPath(){
		return inputPath;
	}

	public String getDfsUri(){
		return dfsUri;
	}

	public void setReducersNumber(int r){
		this.reducersNumber = r;
	}

	public void setInputSplitSize(int size){
		this.inputSplitSize = size;
	}

	public void setBucketName(String name){
		this.bucket = name;
	}

	public void setInputPath(String path){
		this.inputPath = path;
	}

	public void setGraph(data.Graph g){
		this.graph = g;
	}

	public boolean setRoot(State s){
		if(graph != null){
			graph.addNode(s);
			remainings.add(s);
			return true;
		}
		else
			return false;
	}

	public void setLocal(){
		this.local = true;
	}


	public void setMapSlots(int m){
		this.mapSlots  = m;
	}

	public void setFirstIteration(int i){
		this.iteration = i;
	}

	public void hdfsConfig(){

		if(bucket.isEmpty()){
			System.out.println("ERROR: Missing S3 Bucket name");
			System.exit(1);
		}

		if(local){
			dfsUri = "hdfs://localhost:9000/" + this.bucket;
			inputDir = dfsUri + "/input";
			outputDir = dfsUri + "/output";
			reducedDir = dfsUri + "/oldreduced";
		}
		else{
			dfsUri = "s3n://" + this.bucket;
			inputDir = dfsUri + "/input";
			outputDir = dfsUri + "/output";
			reducedDir = dfsUri + "/oldreduced";
		}
	}


	/**
	 * Effettua operazioni preliminari, prima di commissionare un nuovo job:
	 * elimina output dir, se esiste gia'
	 * 
	 * @param job
	 */
	public void cleanUp(Job job){
		try{
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// remove output dir.
			if(fs.exists(FileOutputFormat.getOutputPath(job)))
				fs.delete(FileOutputFormat.getOutputPath(job), true);
		}catch(IOException e){e.printStackTrace();}
	}

	public void cleanInput(Job job){
		try{
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// remove input dir.
			if(fs.exists(FileInputFormat.getInputPaths(job)[0]))
				fs.delete(FileInputFormat.getInputPaths(job)[0], true);
		}catch(IOException e){e.printStackTrace();}
	}

	public void cleanDir(Configuration conf, String dir){
		try{
			Path path=new Path(dir);
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), conf);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// remove dir.
			if(fs.exists(path))
				fs.delete(path, true);
		}catch(IOException e){e.printStackTrace();}
	}


	/**
	 * load TRG and remainings from output files.
	 * @param job
	 * @return
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public void loadOutput(Job job, Class<?> stateClass) throws IOException, InstantiationException, IllegalAccessException{
		graph=new Graph();
		graph.setName("out-graph");
		remainings=new ArrayDeque<State>();
		Path dir=new Path(reducedDir);
		ArrayList<String> oldReduced=new ArrayList<String>();

		FileSystem fs=null;
		try {
			fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SequenceFile.Reader reader=null;
		Text key=null;
		Text value=null;

		// get oldreduced nodes.
		if(fs.exists(dir))
			for(FileStatus fr: fs.listStatus(dir)){
				reader=new SequenceFile.Reader(fs, fr.getPath(), job.getConfiguration());

				key=new Text();
				value = new Text();

				while(reader.next(key, value)){
					oldReduced.add(value.toString());
					key=new Text();
					value = new Text();
				}
				reader.close();
			}

		// create trg.
		for(FileStatus f: fs.listStatus(FileOutputFormat.getOutputPath(job)))
			if(f.getPath().getName().startsWith("part-")){
				reader=new SequenceFile.Reader(fs, f.getPath(), job.getConfiguration());

				key=new Text();
				State val = (State)stateClass.newInstance();

				while(reader.next(key, val)){
					graph.addNode(val);

					if(!val.isExpanded())
						remainings.add(val);

					for(Edge e: new ArrayList<Edge>(val.getIncomingEdges()))
						if(oldReduced.contains(e.getSource()))
							val.removeIncomingEdge(e);

					key=new Text();
					val = (State)stateClass.newInstance();
				}
				reader.close();
			}
	}

	private long getInputSize(Job job){
		FileSystem fs=null;
		long size = 0;
		try {
			fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());

			for(FileStatus f: fs.listStatus(FileInputFormat.getInputPaths(job)))
				if(f.getPath().getName().startsWith("part-"))
					size += f.getLen();

		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch(IOException e){
			e.printStackTrace();
		}

		return size;
	}


	public int run() throws Exception{
		return ToolRunner.run(this, null);
	}
	
	
	/**
	 * load current model from the dfs.
	 * @param conf
	 * @param netPath
	 */
	public Model loadModelFromDfs(Configuration conf, String inputPath, String dfsUri){
		try{
			FileSystem fs=null;
			try {
				fs = FileSystem.get(new URI(dfsUri), conf);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			Path np=new Path(inputPath);
			if(!fs.exists(np)){
				System.out.println("ERROR: Invalid model path");
				System.exit(1);
			}
			else{
				Model model = null;
				try {
					model = modelClass.newInstance();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				InputStream is=fs.open(np);
				model.buildFromFile(is);
				is.close();
				return model;
			}

		}catch(IOException e){e.printStackTrace();}
		return null;
	}
	
	
	private void sequentialEX(){
		HashMap<String, State> states = new HashMap<String, State>();
		ArrayList<State> trueStates = new ArrayList<State>();
		HashMap<String, State> out = new HashMap<String, State>();
		
		Model model = this.loadModelFromDfs(getConf(), getInputPath(), getDfsUri());
		
		System.out.println("Reading input...");
		String inputDir = "/Users/matteo/Desktop/temp/";
		Configuration c = new Configuration();
		try {
			//FileSystem fs = FileSystem.get(new URI(dfsUri), job.getConfiguration());
			for(FileStatus f: FileSystem.getLocal(c).listStatus(new Path(inputDir))){
				if(f.getPath().getName().startsWith("part-")){
					SequenceFile.Reader reader = new SequenceFile.Reader(FileSystem.getLocal(c), f.getPath(), c);
					
					Text k = new Text();
					State v = stateClass.newInstance();
					while(reader.next(k, v)){
						System.out.println("Reading state: " + v.getName());
						states.put(v.getName(), v);
						if(testSubFormula2(model.getPMap(), v))
							trueStates.add(v);
						v = stateClass.newInstance();
					}
					
					reader.close();
				}
			}
			System.out.println("DONE. TIME = " + System.currentTimeMillis());
			System.out.println("Running EX[_]...");
			int k=0;
			int progress = 0;
			for(State i: trueStates){
				System.out.println("Cheking state " + i.getName() + "...");
				if((++k) > 1000){
					progress += k;
					k = 0;
					System.out.println("Progress... " + ((progress*100)/trueStates.size()) + "%.");
				}
//				if(testSubFormula2(model.getPMap(), i)){
					//System.out.println("Yes.");
					for(Edge e: i.getIncomingEdges()){
						State src = states.get(e.getSource());
						System.out.println("Edge from: " + src.getName());
						out.put(src.getName(), src);
					}
//				}
//				else
//					System.out.println("No.");
			}
			System.out.println("Writing output... TIME = " + System.currentTimeMillis());
			SequenceFile.Writer writer = SequenceFile.createWriter(FileSystem.getLocal(c), c, 
					new Path("/Users/matteo/Desktop/temp/out"), Text.class, stateClass);
			for(State n: out.values()){
				System.out.println("Writing " + n.getName());
				writer.append(new Text(n.getFeatures()), stateClass.cast(n));
			}
			writer.close();
			System.out.println("DONE!, " + out.size() + " states written. TIME = " + System.currentTimeMillis());

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} //catch (URISyntaxException e1) {
//			e1.printStackTrace();
//		}
	}
	
	private boolean testSubFormula2(HashMap<String,Integer> pMap, State state){
		/* (|marking(Active)|!=|marking(Memory)|) | (|marking(Queue)|=|marking(Active)|) */
		
		short[] marking = state.get();
		
		short active1 = marking[pMap.get("cId-4690664751711947012104")];
		short active2 = marking[pMap.get("cId-4690664751711947012105")];
		short active3 = marking[pMap.get("cId-4695622201361999396106")];
		short active4 = marking[pMap.get("cId-4690664751711947012107")];
		short active5 = marking[pMap.get("cId-4695622201361999396108")];
		short active6 = marking[pMap.get("cId-4695622201361999396109")];
		short active7 = marking[pMap.get("cId-4693969713850347972110")];
		short active8 = marking[pMap.get("cId-4693969713850347972111")];
		short active9 = marking[pMap.get("cId-4700579651012051780112")];
		short active10 = marking[pMap.get("cId-4692317230633663844113")];
		
		short queue1 = marking[pMap.get("cId-4692317230633663844114")];
		short queue2 = marking[pMap.get("cId-4692317230633663844115")];
		short queue3 = marking[pMap.get("cId-4692317230633663844116")];
		short queue4 = marking[pMap.get("cId-4692317230633663844117")];
		short queue5 = marking[pMap.get("cId-4692317230633663844118")];
		short queue6 = marking[pMap.get("cId-4692317230633663844119")];
		short queue7 = marking[pMap.get("cId-4700579651012051780120")];
		short queue8 = marking[pMap.get("cId-4700579651012051780121")];
		short queue9 = marking[pMap.get("cId-4700579651012051780122")];
		short queue10 = marking[pMap.get("cId-4700579651012051780123")];
		
		short memory1 = marking[pMap.get("cId-462952284262986320394")];
		short memory2 = marking[pMap.get("cId-462787035511821177995")];
		short memory3 = marking[pMap.get("cId-468735978098361146096")];
		short memory4 = marking[pMap.get("cId-468901226420029558897")];
		short memory5 = marking[pMap.get("cId-468735978098361146098")];
		short memory6 = marking[pMap.get("cId-468735978098361146099")];
		short memory7 = marking[pMap.get("cId-4626217871901527650100")];
		short memory8 = marking[pMap.get("cId-4626217871901527650101")];
		short memory9 = marking[pMap.get("cId-4624565392979810818102")];
		short memory10 = marking[pMap.get("cId-4624565392979810818103")];
		
		
		return eval2(active1, memory1, queue1) &&
				eval2(active2, memory2, queue2) &&
				eval2(active3, memory3, queue3) &&
				eval2(active4, memory4, queue4) &&
				eval2(active5, memory5, queue5) &&
				eval2(active6, memory6, queue6) &&
				eval2(active7, memory7, queue7) &&
				eval2(active8, memory8, queue8) &&
				eval2(active9, memory9, queue9) &&
				eval2(active10, memory10, queue10);
	}
	
	private boolean eval2(short active, short memory, short queue){
		return active != memory || queue == active;
	}
	
	

	public int run(String [] args) throws Exception {
		
		System.out.println("START TIME = " + System.currentTimeMillis());

		this.hdfsConfig();
	
//		sequentialEX(); // SEQ. ALGORITM TEST !!
//		System.exit(0);
		
		DistributedCache.addCacheFile(new URI(getInputPath()), getConf());
		int i= iteration;

		Job job = new Job(getConf());
		job.setJarByClass(EXLauncher.class);
		job.setJobName("ctlex");

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(stateClass);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		job.setMapperClass(EXMapper.class);
		job.setReducerClass(EXReducer.class);

		job.setInputFormatClass(SequenceFileInputFormat.class);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);

		//SequenceFileInputFormat.setMinInputSplitSize(job, size);
		job.setNumReduceTasks(reducersNumber);
		FileInputFormat.setInputPaths(job, inputDir + i + "/*");
		FileOutputFormat.setOutputPath(job, new Path(inputDir + (i+1)));
		if(mapSlots != 0)
			inputSplitSize = this.getInputSize(job) / mapSlots;
		SequenceFileInputFormat.setMaxInputSplitSize(job, inputSplitSize);

		System.out.println("\n**************************");
		System.out.println(" CTL FORMULA EX[_] ON INPUT: " + i);
		System.out.println("**************************");
		cleanDir(job.getConfiguration(), outputDir);
		job.getConfiguration().set(STATE_CLASS_NAME, stateClass.getName());
		job.getConfiguration().set(MODEL_CLASS_NAME, modelClass.getName());

		boolean success = job.waitForCompletion(true);
		//this.createDeadlockFile(job);

		System.out.println("STOP: "+System.currentTimeMillis());


		return success ? 0 : 1;
	}

} 
