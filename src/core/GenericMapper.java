/**
 * This file is part of Mardigras.
 * 
 *  Mardigras is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Mardigras is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Mardigras. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import data.Edge;
import data.Model;
import data.State;

import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class GenericMapper<E extends State> extends Mapper<Text, E, Text, E> {
	
	//private File inputFile;
	private data.Model model;
	private Class<? extends Edge> edgeClass;
	private HashMap<Text, State> map;
	
	private boolean imc = false;
	
	private int mapOutNewNodesCounter;
	private int mapOutOldNodesCounter;
	
	/**
	 * Get the input file form the distributed cache.
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		Path[] localFiles = DistributedCache.getLocalCacheFiles(context.getConfiguration());
		File inputFile = new File(localFiles[0].toString());
		
		InputStream in = new FileInputStream(inputFile);
		String modelClassName = context.getConfiguration().get(GenericGraphgen.MODEL_CLASS_NAME);
		String edgeClassName = context.getConfiguration().get(GenericGraphgen.EDGE_CLASS_NAME);
		model = null;
		try {
			edgeClass = (Class<? extends Edge>) Class.forName(edgeClassName);
			model = (Model) Class.forName(modelClassName).newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		model.buildFromFile(in);
		in.close();
		
		imc = context.getConfiguration().getBoolean(GenericGraphgen.IMC, false);
		if(imc)
			map = new HashMap<Text, State>();
		
		mapOutNewNodesCounter = 0;
		mapOutOldNodesCounter = 0;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void map(Text key, E value, Context context)  throws IOException, InterruptedException{
		
		if(!value.isExpanded()){
			//this.incNewStates(context);
			for(State n: value.createSuccessors(getID(context), model)){
				   try {
					   Edge e = edgeClass.newInstance();
					   e.setSource(value);
					   n.addIncomingEdge(e);
					   e.addLabel(value, n);
				   } catch (InstantiationException e) {
					   e.printStackTrace();
				   } catch (IllegalAccessException e) {
						e.printStackTrace();
				   }
				   
				   if(!imc){
					   mapOutNewNodesCounter++;
					   context.write(new Text(n.getFeatures()), (E)n);
				   }else{
					   Text k = new Text(n.getFeatures());
					   State e = map.get(k);
					   if(e == null)
						   map.put(k, n);
					   else
						   e.addIncomingEdges(n.getIncomingEdges());
				   }
					   
			}
			value.setExpanded();
		}
		
		if(!imc){
			mapOutOldNodesCounter++;
			context.write(key, value);
		} else {
			   State e = map.get(key);
			   if(e == null)
				   map.put(key, (E)value.clone());
			   else{
				   e.addIncomingEdges(value.getIncomingEdges());
				   e.setExpanded();
			   }
		}
		
	}
	
	
	@Override
	protected void cleanup(Context context) throws IOException, InterruptedException {
		//super.cleanup(context);
		
//		if(imc){
//			for(Text k: map.keySet())
//				context.write(k, (E)map.get(k));
//		}
		
		context.getCounter(GenericGraphgen.MardigrasCounter.MAP_OUT_NEW_NODES).increment(mapOutNewNodesCounter);
		context.getCounter(GenericGraphgen.MardigrasCounter.MAP_OUT_OLD_NODES).increment(mapOutOldNodesCounter);
	}

	/**
	 * Return a unic id for this map task.
	 * @param context
	 * @return
	 */
	public String getID(Context context){
		   String taskAttemptID=context.getTaskAttemptID().toString();
		   String[] token=taskAttemptID.split("_");
		   return new Integer(token[token.length-4])+ "_" + new Integer(token[token.length-2])+ "_";
	   }
}
